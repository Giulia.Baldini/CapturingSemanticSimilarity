Thesis outline:
Abstract
Introduction: talk about the problem without going into details.
Motivation: what is the problem
Contribution: what I plan to do 
Related work: highly related area to this work is the NLP. More specifically, many approaches to represent text for finding similarity between words. Mention wordnet, word2vec, .... After describing what exists, than I have to link what exists with what I am planning to do. (in a short sentence)
Approach:
Data pre-processing: Triple cleansing (why it is done, how triples were)
Finding Synonyms
WordNet (how we use it to retrieve synonyms, talk about the options)
Word Vectors (how we use them to retrieve synonyms)
Word2vec
Spacy
Not only synonyms but also scale learning with Word Vectors. Scale learning is the power of word vectors and it can help us find relations with adjectives.
Query Expansion: how to generate triples set, ask the system, how results are merged.
Evaluation (show retrieved synonyms in same graphs, so that we have an easy comparison. Shows evaluation stand alone and with ground truth (quality of synonyms, recall and precision. Which method was best and why, how many reviews more do we find and how useful they are. Do again Aaron’s evaluation with the improvements and check the results.
Conclusion: we implemented this and that, and as we’ve seen in the evaluation in works. Give quality statement on how good it is and for what.
Future work: how to better improve the system. Languages, different approaches etc.
