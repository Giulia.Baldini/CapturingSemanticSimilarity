\chapter{Related Work}
\label{relatedwork}
Our work can be located in the the \emph{Natural Language Processing (NLP)} area. In this section we discuss all the technologies that aim to find similarity between words.

\section{\wordnet}
\label{relatedwork:wordnet}
\wordnet\cite{Miller1995, Fellbaum1998} is a lexical database where each English word can be defined with many \emph{synonym sets (synsets)} in the \wordnet vocabulary, which are used to capture the meaning of that word in different contexts. Looking at the words contained in a synset, it is possible to find word synonyms. For example we can have a look at the results given for the word \emphq{nice} (\autoref{fig:wordnet}). It can be a noun (and be a city in France), or can be an adjective and mean pleasant or socially or conventionally correct.

\begin{figure}
	\begin{center}
		\includegraphics[width=10cm]{wordnet.png}
	\end{center}
	\caption{Results of the search for the word \emph{nice} on \wordnet \cite{Princeton2010}.}
	\label{fig:wordnet}
\end{figure}

\autoref{fig:wordnettree} shows the standard structure for an adjective in this database. Every word has many synsets, which correspond to the different semantic meanings that a word can have. Each synset is a set of lemmas with related meanings. A lemma is the canonical form of a word, such as \emphq{thief} is the lemma of \emphq{thieves}. Lemmas belonging to the same synset can be seen as actual synonyms. Looking at our example in \autoref{fig:wordnet}, \emphq{decent} and \emphq{nice} are lemmas for the second synset.\\
A synset is named after its most common lemma. Since more than one synset might have the same lemma, a \emph{sense} is added to the name. The sense is an integer identifying the frequency of use of a synset, where lower numbers indicate a high frequency. The most common sense is numbered one.
\begin{figure}
	\centering
	\begin{forest}
		for tree={
			grow'=0,
			child anchor=west,
			parent anchor=south,
			anchor=west,
			calign=first,
			edge path={
				\noexpand\path [draw, \forestoption{edge}]
				(!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
			},
			before typesetting nodes={
				if n=1
				{insert before={[,phantom]}}
				{}
			},
			fit=band,
			before computing xy={l=15pt},
		}
		[Word
		[Synset
		[Lemmas]
		[Attributes
		[See Also]
		[Similar To]
		[Attribute]
		[Antonyms]
		[Derivationally Related Forms]
		]
		]	
		[...]
		]
	\end{forest}
	\caption{The structure of the \wordnet lexical database.}
	\label{fig:wordnettree}
\end{figure}
Synsets also have attributes; each of these might exist or not depending on the synset:
\begin{itemize}
	\item The \emphq{see also} attribute refers to synsets which do not contain the original word as lemma but have the same meaning. For example, \emphq{good} has \emphq{best} as one of its \emphq{see also} synsets.
	\item The \emphq{similar to} attribute contains synsets that are related to the synset. For example, \emphq{satisfactory} is a \emphq{similar to} synset of \emphq{good}.
	\item The \emphq{attribute} refers to the quality that the synset describes. For example, for \emphq{big} it is \emphq{size}.
	\item The \emphq{antonym} attribute contains all the words that are antonyms of that specific synset.
	\item The \emphq{derivationally related forms} are terms that have the same root forms and are semantically related. For example, \emphq{largeness} is a \emphq{derivationally related form} of \emphq{large}.
\end{itemize}

\wordnet has a similarity function for verbs and nouns. In fact, those have a hierarchical structure which allows \wordnet to calculate the shortest path between two words. This path is calculated with different measures, as it can be seen in \autoref{fig:wordnetsimilarity}.

\begin{figure}
	\begin{center}
		\includegraphics[width=10cm]{wordnetsimilarity.png}
	\end{center}
	\caption{Search of the similarity between \emph{thief} and \emph{robber} in \wordnet \cite{Shima2013}.}
	\label{fig:wordnetsimilarity}
\end{figure}

Unfortunately, adjectives are not organised in this structure, and thus the results of these similarity functions yield nothing. The only two measures that can be retrieved are the \emph{Lesk} \cite{Lesk1986} and the \emph{Hirst and St-Onge (HSO)} \cite{Hirst1997} measures. The Lesk measure finds how many overlaps there are between the \wordnet definitions of the two words. The HSO measure identifies links between the \wordnet synsets of the two words. However, the computational cost for both these measures is quite high and they rely heavily on the synsets definitions.\\
It is also possible to convert adjectives to nouns using \emphq{derivationally related forms}, and then use the similarity functions mentioned before. However, many synsets do not have these forms, so in practice it is not possible to retrieve many similar adjectives.

\section{\sentiwordnet}
\label{relatedwork:sentiwordnet}
\sentiwordnet is a lexical resource for opinion mining \cite{Esuli2006, Baccianella2010}. It builds on \wordnet by assigning three sentiment scores to each synset: positivity, negativity, objectivity. All three scores are $\in [0, 1]$ and sum up to 1, but only positivity and negativity are defined; objectivity is calculated as $1 - positivity - negativity$. The scores describe how objective, positive, and negative the terms contained in the synset are.\\
This tool adds a second power to \wordnet; it is now possible to order words according to their polarity (that is, according to these scores). We will show a way to order reviews based on this technology.

\section{\wordvec}
\label{relatedwork:word2vec}
In language modeling, word vectors represent words or phrases in a multidimensional space. \wordvec \cite{Mikolov2013} gives the possibility to create word-vector models with sentences as an input, which influence heavily the positions of the vectors in the space. Since it assigns a vector to each word, they all start from the same point, which by definition is  $0^d$ where $d$ is the number of dimensions.\\
This system uses the \emph{cosine similarity} to measure the similarity between two vectors. The positioning of the words in space and therefore their similarity is based on the context in which they appear. For example, in the sentences \emphq{the breakfast at the hotel was good} and \emphq{the breakfast at the hotel was bad}, the words good and bad are used in the same way, which means that they will appear as similar. The concept of cosine similarity comes from the \emph{dot product} or \emph{scalar product}.\\
The scalar product between two vectors $\vec{a}$ and $\vec{b}$ is defined as:
\begin{linenomath*}
\begin{ceqn}
	\begin{align}
	\label{eqscal1}
	\vec{a} \cdot \vec{b} = ||\vec{a}|| \cdot || \vec{b} || \cdot \cos(\theta)
	\end{align}
\end{ceqn}
\end{linenomath*}
and thus their cosine is:
\begin{linenomath*}
\begin{ceqn}
	\begin{align}
	\label{eqscal2}
	\text{Cosine Similarity} = \cos(\theta) = \frac{\vec{a} \cdot \vec{b}}{||\vec{a}|| \cdot || \vec{b} ||} 
	\end{align}
\end{ceqn}
\end{linenomath*}

The positions of the vectors with respect to each other can be grouped in three cases (\autoref{fig:vectors}):
\begin{itemize}
	\item Two vectors are related when they point in a similar direction, as in \autoref{fig:vectorsa}. In this case, $cos(\theta)$ is very close to 1. We can consider two related vectors synonyms.
	\item Two vectors are unrelated if the angle between them is $90^\circ$ (\autoref{fig:vectorsb}). In such case  $cos(\theta)$ will be very close to 0. These vectors have very different meaning or context.
	\item Two vectors are opposites if the angle between them is close to $180^\circ$ and so $cos(\theta)$ is close to -1 (\autoref{fig:vectorsc}). In this case the vectors might have something in common, but in an opposite fashion.
\end{itemize}

After this analysis we can conclude that the Cosine Similarity $cos(\theta) \in [-1,1]$.

\begin{figure}
\centering
\begin{subfigure}{0.31\textwidth}
\begin{tikzpicture}[yscale=0.27, xscale=0.27]
\draw[thin,gray!40] (-8,-8) grid (8,8);
\draw[<->] (-8,0)--(8,0) node[right]{$x$};
\draw[<->] (0,-8)--(0,8) node[above]{$y$};
\draw[line width=2pt,colorone,-stealth](0,0)--(3,6) node[anchor=north west]{$\boldsymbol{a}$};
\draw[line width=2pt,colortwo,-stealth](0,0)--(6,3) node[anchor=south west]{$\boldsymbol{b}$};
\draw (1,1.7) arc (50:22:2);
\node[] at (45:3)  {$\theta$};
\end{tikzpicture}
\subcaption{Related vectors.}
\label{fig:vectorsa}
\end{subfigure}
\begin{subfigure}{0.31\textwidth}
\begin{tikzpicture}[yscale=0.27, xscale=0.27]
\draw[thin,gray!40] (-8,-8) grid (8,8);
\draw[<->] (-8,0)--(8,0) node[right]{$x$};
\draw[<->] (0,-8)--(0,8) node[above]{$y$};
\draw[line width=2pt,colorone,-stealth](0,0)--(-3,6) node[anchor=north east]{$\boldsymbol{a}$};
\draw[line width=2pt,colortwo,-stealth](0,0)--(6,3) node[anchor=south west]{$\boldsymbol{b}$};
\draw (-0.38, 1)--(0.4, 1.39);
\draw (0.4, 1.39)--(0.82, 0.55);
\node[] at (60:3)  {$\theta$};
\end{tikzpicture}
\subcaption{Unrelated vectors.}
\label{fig:vectorsb}
\end{subfigure}
\begin{subfigure}{0.31\textwidth}
\begin{tikzpicture}[yscale=0.27, xscale=0.27]
\draw[thin,gray!40] (-8,-8) grid (8,8);
\draw[<->] (-8,0)--(8,0) node[right]{$x$};
\draw[<->] (0,-8)--(0,8) node[above]{$y$};
\draw[line width=2pt,colorone,-stealth](0,0)--(-6,-2) node[anchor=north east]{$\boldsymbol{a}$};
\draw[line width=2pt,colortwo,-stealth](0,0)--(6,3) node[anchor=south west]{$\boldsymbol{b}$};
\draw (-2,-0.54) arc (180:44:2);
\node[] at (105:2.5)  {$\theta$};
\end{tikzpicture}
\subcaption{Opposite vectors.}
\label{fig:vectorsc}
\end{subfigure}
\caption{Positions in which the vectors might be with respect to each other.}
\label{fig:vectors}
\end{figure}

\section{\spacy}
\label{relatedwork:spacy}
\spacy\footnote{\href{https://spacy.io}{https://spacy.io/}} is based on \wordvec and provides many useful tools in the NLP area. By default, it uses 300-dimensional vectors trained on the Common Crawl\footnote{\href{https://commoncrawl.org/}{https://commoncrawl.org/}} corpus using the GloVe algorithm \cite{Pennington2014}. Like \wordvec, it also measures similarity by calculating the cosine between word vectors.\\\\
Our work can be seen as a similarity quest too; in fact, we will use each of these systems to find similarity or synonyms between adjectives in the context of features.